import * as React from 'react';
import { HomePage } from 'app/containers/HomePage';
import { Root } from 'app/containers/Root';
import { hot } from 'react-hot-loader/root';
import { Router, Switch } from 'react-router';
import { ProtectedRoute, AuthenticatedRoute } from 'app/util';
import { LoginPage } from './containers/LoginPage';
import { inject, observer } from 'mobx-react';
import { STORE_USER } from './constants';
import { UserStore } from './stores';
import { History } from 'history';
import { Route } from 'react-router-dom';
import { ForbiddenPage } from './containers/ForbiddenPage';
import { NotFoundPage } from './containers/NotFoundPage';
import { ProfilePage } from './containers/ProfilePage';
import { LoginCallbackPage } from './containers/LoginCallbackPage';

export interface AppProps {
  history: History<any>;
}

@hot
@inject(STORE_USER)
@observer
export class App extends React.Component<AppProps> {
  public render() {
    const userStore = this.props[STORE_USER] as UserStore;
    return (
      <Root>
        <Router history={this.props.history}>
          <Switch>
            <AuthenticatedRoute exact path="/dashboard"
              isAuthenticated={userStore.isAuthenticated}
              isAllowed={true}
              authenticationPath="/login"
              restrictedPath="/forbidden"
              component={HomePage} />
            <AuthenticatedRoute exact path="/settings"
              isAuthenticated={userStore.isAuthenticated}
              isAllowed={true}
              authenticationPath="/login"
              restrictedPath="/forbidden"
              component={ProfilePage} />
            <ProtectedRoute exact path="/login"
              isAllowed={!userStore.isAuthenticated}
              restrictedPath="/dashboard"
              component={LoginPage} />
            <ProtectedRoute path="/login/callback"
              isAllowed={!userStore.isAuthenticated}
              restrictedPath="/dashboard"
              component={LoginCallbackPage} />
            <Route
              path="/forbidden"
              component={ForbiddenPage} />
            <Route
              component={NotFoundPage} />
          </Switch>
        </Router>
      </Root>
    );
  }
}

export default App;
