import { observable, computed, runInAction } from 'mobx';
import { API } from 'app/api';
import { UserModel } from 'app/model/user';
import Cookies from 'js-cookie';
import jwt from 'jsonwebtoken';

export class UserStore {
  @observable
  public model: UserModel;

  public constructor() {
    this.model = new UserModel();
    this.isAuthenticated = JSON.parse(localStorage.getItem('isAuthenticated'));

    API.post('/auth/verify')
      .then(() => {
          /* tslint:disable: no-string-literal */
        const token = Cookies.get('OmniPanelUser');
        const username = jwt.decode(token)['user'].username;
        this.username = username;
        this.isAuthenticated = true;
      })
      .catch(() => this.isAuthenticated = false);
  }

  @computed
  public get username(): string { return this.model.username; }
  public set username(value: string) { runInAction('set username', () => this.model.username = value); }

  @computed
  public get isAuthenticated(): boolean { return this.model.isAuthenticated; }
  public set isAuthenticated(value: boolean) { runInAction('set isAuthenticated', () => {
    this.model.isAuthenticated = value;
    localStorage.setItem('isAuthenticated', JSON.stringify(value));
  }); }
}
