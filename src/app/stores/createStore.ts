import { STORE_ROUTER, STORE_USER } from 'app/constants';
import { RouterStore } from 'app/stores';
import { History } from 'history';
import { UserStore } from './userStore';

export function createStores(history: History) {
  const routerStore = new RouterStore(history);
  const userStore = new UserStore();

  return {
    [STORE_ROUTER]: routerStore,
    [STORE_USER]: userStore,
  };
}
