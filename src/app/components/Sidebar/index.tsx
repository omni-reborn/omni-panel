import * as React from 'react';
import * as style from './style.css';
import { SidebarButton } from './SidebarButton';

export class Sidebar extends React.Component {
  public render() {
    return (
      <div className={style.sidebar}>
        <SidebarButton icon="home" text="Dashboard" path="/dashboard" />
        <SidebarButton icon="home" text="Commands" path="/commands" />
        <SidebarButton icon="home" text="Servers" path="/servers" />
        <SidebarButton icon="home" text="Settings" path="/settings" />
      </div>
    );
  }
}