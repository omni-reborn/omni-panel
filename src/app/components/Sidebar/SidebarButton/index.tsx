import * as React from 'react';
import * as classnames from 'classnames/bind';
import * as style from './style.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { inject, observer } from 'mobx-react';
import { STORE_ROUTER } from 'app/constants';
import { RouterStore } from 'app/stores';

const cx = classnames.bind(style);

export interface SidebarButtonProps {
  icon: IconProp;
  text: string;
  path: string;
}

@inject(STORE_ROUTER)
@observer
export class SidebarButton extends React.Component<SidebarButtonProps> {

  private handleClick() {
    const routerStore = this.props[STORE_ROUTER] as RouterStore;
    if (routerStore.location.pathname !== this.props.path) {
      routerStore.push(this.props.path);
    }
  }

  public render() {
    const routerStore = this.props[STORE_ROUTER] as RouterStore;

    const buttonClassnames = cx({
      button: true,
      active: routerStore.location.pathname === this.props.path,
    });

    return (
      <div className={buttonClassnames} onClick={() => this.handleClick()}>
        <FontAwesomeIcon className={style.icon} icon={this.props.icon} />
        <span>{this.props.text}</span>
      </div>
    );
  }
}
