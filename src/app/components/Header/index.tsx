import * as React from 'react';
import * as style from './style.css';
import * as classnames from 'classnames/bind';
import { inject, observer } from 'mobx-react';
import { STORE_ROUTER, STORE_USER } from 'app/constants';
import { RouterStore, UserStore } from 'app/stores';
import { toast } from 'react-toastify';
import { API } from 'app/api';
import Logo from 'assets/images/owl.svg';
import UserPlaceHolder from 'assets/images/user-placeholder.svg';
import DownArrow from 'assets/images/arrow-down.svg';
import PowerIcon from 'assets/images/power.svg';
import SettingsIcon from 'assets/images/settings.svg';

const cx = classnames.bind(style);

export interface HeaderState {
  dropdownActive: boolean;
}

@inject(STORE_ROUTER, STORE_USER)
@observer
export class Header extends React.Component<{}, HeaderState> {
  public state = {
    dropdownActive: false,
  };

  private async handleLogout() {
    const routerStore = this.props[STORE_ROUTER] as RouterStore;
    const userStore = this.props[STORE_USER] as UserStore;

    try {
      await API.post('/auth/logout', {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      });

      userStore.isAuthenticated = false;
      routerStore.push('/login');
    } catch (error) {
      toast.error(error.response.data.error);
    }
  }

  public render() {
    const routerStore = this.props[STORE_ROUTER] as RouterStore;

    const userDropdownClassnames = cx({
      userDropdown: true,
      hidden: !this.state.dropdownActive,
    });

    return (
      <div className={style.header}>
        <Logo className={style.logo} viewBox="0 0 508 508" />
        <span className={style.title}><span className={style.bold}>Omni</span> Reborn</span>
        <div className={style.userContainer}
          onClick={() => this.setState({ dropdownActive: !this.state.dropdownActive })}>
          <UserPlaceHolder className={style.userImage} viewBox="0 0 311.541 311.541" />
          <DownArrow className={style.downArrow} viewBox="0 0 451.847 451.847" />
          <div className={userDropdownClassnames}>
            <a onClick={() => routerStore.push('/settings')}>
              <SettingsIcon className={style.icon} viewBox="0 0 512 512" />
              <span>Settings</span>
            </a>
            <a onClick={() => this.handleLogout()}>
              <PowerIcon className={style.icon} viewBox="0 0 489.888 489.888" />
              <span>Log Out</span>
            </a>
          </div>
        </div>
      </div>
    );
  }
}
