import * as React from 'react';
import * as classnames from 'classnames/bind';
import * as style from './style.css';

const cx = classnames.bind(style);

export interface TextInputProps {
  className?: string;
  spellCheck?: boolean;
  isError?: boolean;
  inputRef?: React.RefObject<HTMLInputElement>;
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
}

export class TextInput extends React.Component<TextInputProps> {
  public render() {
    const inputClassnames = cx({
      textInput: true,
      [this.props.className]: true,
      error: this.props.isError,
    });

    return (
      <input className={inputClassnames} type="text"
        ref={this.props.inputRef}
        spellCheck={this.props.spellCheck}
        onChange={(e) => this.props.onChange(e)}
      />
    );
  }
}
