import * as React from 'react';
import * as classnames from 'classnames/bind';
import * as style from './style.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEye, faEyeSlash } from '@fortawesome/free-regular-svg-icons';

const cx = classnames.bind(style);

export interface PasswordInputProps {
  className?: string;
  spellCheck?: boolean;
  isError?: boolean;
  inputRef?: React.RefObject<HTMLInputElement>;
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
}

export interface PasswordInputState {
  visible: boolean;
}

export class PasswordInput extends React.Component<PasswordInputProps> {
  public state = {
    visible: false,
  };

  private handleEyeClick(e: React.MouseEvent<SVGSVGElement>) {
    e.preventDefault();
    this.setState({ visible: !this.state.visible });
  }

  public render() {
    const containerClassnames = cx({
      inputContainer: true,
      [this.props.className]: true,
    });

    const inputClassnames = cx({
      passwordInput: true,
      error: this.props.isError,
    });

    return (
      <div className={containerClassnames}>
        <input className={inputClassnames} type={this.state.visible ? 'text' : 'password'}
          ref={this.props.inputRef}
          spellCheck={this.props.spellCheck}
          onChange={(e) => this.props.onChange(e)}>
        </input>
        {
          this.state.visible
            ? <FontAwesomeIcon className={style.iconInvisible} icon={faEye} onClick={(e) => this.handleEyeClick(e)} onMouseDown={(e) => e.preventDefault()} />
            : <FontAwesomeIcon className={style.iconVisible} icon={faEyeSlash} onClick={(e) => this.handleEyeClick(e)} onMouseDown={(e) => e.preventDefault()} />
        }
      </div>
    );
  }
}
