import * as React from 'react';
import * as jwt from 'jsonwebtoken';
import * as style from './style.css';
import { TextInput } from 'app/components/UIElements/TextInput';
import { PasswordInput } from 'app/components/UIElements/PasswordInput';
import { STORE_USER } from 'app/constants';
import { observer, inject } from 'mobx-react';
import { UserStore } from 'app/stores';
import { toast } from 'react-toastify';
import { API } from 'app/api';
import qs from 'qs';
import Cookies from 'js-cookie';

export interface UpdateUsernameFormState {
  username: string;
  password: string;
  usernameFailMessage: string;
  passwordFailMessage: string;
}

@inject(STORE_USER)
@observer
export class UpdateUsernameForm extends React.Component<{}, UpdateUsernameFormState> {
  private readonly usernameField: React.RefObject<HTMLInputElement>;
  private readonly passwordField: React.RefObject<HTMLInputElement>;

  public state = {
    username: '',
    password: '',
    usernameFailMessage: '',
    passwordFailMessage: '',
  };

  public constructor(props: {}, state: UpdateUsernameFormState) {
    super(props, state);
    this.usernameField = React.createRef();
    this.passwordField = React.createRef();
  }

  private handleUsernameChange(e: React.ChangeEvent<HTMLInputElement>) {
    const userStore = this.props[STORE_USER] as UserStore;
    const input = e.currentTarget.value;
    this.setState({ username: input });

    if (input === userStore.username) {
      this.setState({ usernameFailMessage: 'Username cannot be the same as current one.' });
      return;
    }

    this.setState({ usernameFailMessage: '' });
  }

  private async handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    const userStore = this.props[STORE_USER] as UserStore;
    e.preventDefault();

    if (this.state.username === userStore.username) {
      this.setState({ usernameFailMessage: 'Username cannot be the same as current one.' });
      return;
    }

    try {
      const response = await API.put('/user/username', qs.stringify({ newUsername: this.state.username, password: this.state.password }), {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      });

      /* tslint:disable: no-string-literal */
      const token = Cookies.get('OmniPanelUser');
      const username = jwt.decode(token)['user'].username;

      this.setState({ usernameFailMessage: '', passwordFailMessage: '' });
      this.usernameField.current.value = '';
      this.passwordField.current.value = '';
      userStore.username = username;
      toast(response.data.message, { className: style.toast });
    } catch (error) {
      this.setState({ passwordFailMessage: error.response.data.error });
    }
  }

  public render() {
    return (
      <form className={style.usernameForm} onSubmit={(e) => this.handleSubmit(e)}>
        <span>New Username</span>
        <TextInput spellCheck={false}
          className={style.inputField}
          inputRef={this.usernameField}
          isError={this.state.usernameFailMessage !== ''}
          onChange={(e) => this.handleUsernameChange(e)} />
        <span>Password</span>
        <PasswordInput spellCheck={false}
          className={style.inputField}
          inputRef={this.passwordField}
          isError={this.state.passwordFailMessage !== ''}
          onChange={(e) => this.setState({ password: e.currentTarget.value, passwordFailMessage: '' })} />
        <span className={style.errorMessage}>{this.state.passwordFailMessage !== '' ? this.state.passwordFailMessage : this.state.usernameFailMessage }</span>
        <button>Change Username</button>
      </form>
    );
  }
}
