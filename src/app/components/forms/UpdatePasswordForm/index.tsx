import * as React from 'react';
import * as style from './style.css';
import { PasswordInput } from 'app/components/UIElements/PasswordInput';
import { STORE_USER } from 'app/constants';
import { observer, inject } from 'mobx-react';
import { API } from 'app/api';
import { toast } from 'react-toastify';
import qs from 'qs';

export interface UpdatePasswordFormState {
  oldPassword: string;
  newPassword: string;
  repeatNewPassword: string;
  oldPasswordFailMessage: string;
  repeatPasswordFailMessage: string;
}

@inject(STORE_USER)
@observer
export class UpdatePasswordForm extends React.Component<{}, UpdatePasswordFormState> {
  private readonly oldPasswordInput: React.RefObject<HTMLInputElement>;
  private readonly newPasswordInput: React.RefObject<HTMLInputElement>;
  private readonly repeatNewPasswordInput: React.RefObject<HTMLInputElement>;

  public state = {
    oldPassword: '',
    newPassword: '',
    repeatNewPassword: '',
    oldPasswordFailMessage: '',
    repeatPasswordFailMessage: '',
  };

  public constructor(props: {}, state: UpdatePasswordFormState) {
    super(props, state);
    this.oldPasswordInput = React.createRef();
    this.newPasswordInput = React.createRef();
    this.repeatNewPasswordInput = React.createRef();
  }

  private handleNewPasswordChange(e: React.ChangeEvent<HTMLInputElement>) {
    const input = e.currentTarget.value;
    this.setState({ newPassword: input });

    if (input !== this.state.repeatNewPassword) {
      this.setState({ repeatPasswordFailMessage: 'Repeated password does not match.' });
      return;
    }

    this.setState({ repeatPasswordFailMessage: '' });
  }

  private handleRepeatNewPasswordChange(e: React.ChangeEvent<HTMLInputElement>) {
    const input = e.currentTarget.value;
    this.setState({ repeatNewPassword: input });

    if (input !== this.state.newPassword) {
      this.setState({ repeatPasswordFailMessage: 'Repeated password does not match.' });
      return;
    }

    this.setState({ repeatPasswordFailMessage: '' });
  }

  private async handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();

    if (this.state.newPassword !== this.state.repeatNewPassword) {
      this.setState({ repeatPasswordFailMessage: 'Repeated password does not match.' });
      return;
    }

    try {
      const response = await API.put('/user/password', qs.stringify({ oldPassword: this.state.oldPassword, newPassword: this.state.newPassword }), {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      });

      this.oldPasswordInput.current.value = '';
      this.newPasswordInput.current.value = '';
      this.repeatNewPasswordInput.current.value = '';
      toast(response.data.message, { className: style.toast });
    } catch (error) {
      this.setState({ oldPasswordFailMessage: error.response.data.error });
    }
  }

  public render() {
    return (
      <form className={style.passwordForm} onSubmit={(e) => this.handleSubmit(e)}>
        <span>Old password</span>
        <PasswordInput spellCheck={false}
          className={style.inputField}
          inputRef={this.oldPasswordInput}
          isError={this.state.oldPasswordFailMessage !== ''}
          onChange={(e) => this.setState({ oldPassword: e.currentTarget.value, oldPasswordFailMessage: '' })} />
        <span>New password</span>
        <PasswordInput spellCheck={false}
          className={style.inputField}
          inputRef={this.newPasswordInput}
          isError={this.state.repeatPasswordFailMessage !== ''}
          onChange={(e) => this.handleNewPasswordChange(e)} />
        <span>Repeat new password</span>
        <PasswordInput spellCheck={false}
          className={style.inputField}
          inputRef={this.repeatNewPasswordInput}
          isError={this.state.repeatPasswordFailMessage !== ''}
          onChange={(e) => this.handleRepeatNewPasswordChange(e)} />
        <span className={style.errorMessage}>{this.state.oldPasswordFailMessage !== '' ? this.state.oldPasswordFailMessage : this.state.repeatPasswordFailMessage}</span>
        <button>Change Password</button>
      </form>
    );
  }
}
