import axios, { AxiosRequestConfig } from 'axios';

export abstract class API {
  public static post(route: string, body?: any, config?: AxiosRequestConfig) {
    const uri = 'http://localhost:3000';

    return axios.post(uri + route, body, {
      responseType: 'json',
      withCredentials: true,
      ...config,
    });
  }

  public static get(route: string, config?: AxiosRequestConfig) {
    const uri = 'http://localhost:3000';

    return axios.get(uri + route, {
      responseType: 'json',
      withCredentials: true,
      ...config,
    });
  }

  public static put(route: string, body?: any, config?: AxiosRequestConfig) {
    const uri = 'http://localhost:3000';

    return axios.put(uri + route, body, {
      responseType: 'json',
      withCredentials: true,
      ...config,
    });
  }
}
