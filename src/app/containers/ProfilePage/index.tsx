import * as React from 'react';
import * as style from './style.css';
import { BaseContainer } from 'app/containers/BaseContainer';
import UserPlaceholder from 'assets/images/user-placeholder-square.svg';
import { inject, observer } from 'mobx-react';
import { STORE_USER } from 'app/constants';
import { UserStore } from 'app/stores';
import { capitalizeFirstLetter, isNil } from 'app/util';
import { UpdateUsernameForm } from 'app/components/forms/UpdateUsernameForm';
import { UpdatePasswordForm } from 'app/components/forms/UpdatePasswordForm';

export interface ProfilePageState {
  oldPassword: string;
  newPassword: string;
  newRepeatedPassword: string;
  isErrorUsernamePlate: boolean;
  isErrorPasswordPlate: boolean;
}

@inject(STORE_USER)
@observer
export class ProfilePage extends React.Component<{}, ProfilePageState> {
  public state = {
    oldPassword: '',
    newPassword: '',
    newRepeatedPassword: '',
    isErrorUsernamePlate: false,
    isErrorPasswordPlate: false,
  };

  public render() {
    const userStore = this.props[STORE_USER] as UserStore;

    return (
      <BaseContainer>
        <div className={style.leftColumnContainer}>
          <div className={style.userPlate}>
            <UserPlaceholder className={style.userImage} viewBox="-43 45.5 311.5 311.5" />
            <div className={style.userInfo}>
              <span>{!isNil(userStore.username) ? capitalizeFirstLetter(userStore.username) : 'Omni Panel User'}</span>
              <span>All servers</span>
              <span>Omnibot Creator</span>
            </div>
          </div>
          <div className={style.plate}>
            <span>Edit Username</span>
            <UpdateUsernameForm />
          </div>
          <div className={style.plate}>
            <span>Edit Password</span>
            <UpdatePasswordForm />
          </div>
        </div>
      </BaseContainer>
    );
  }
}
