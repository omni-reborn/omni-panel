import * as React from 'react';
import * as style from './style.css';
import Logo from 'assets/images/owl.svg';
import { STORE_ROUTER } from 'app/constants';
import { observer, inject } from 'mobx-react';

@inject(STORE_ROUTER)
@observer
export class LoginPage extends React.Component {
  public async login() {
    window.location.href = 'https://discordapp.com/api/oauth2/authorize?client_id=648606874752516107&redirect_uri=http%3A%2F%2Flocalhost%3A3001%2Flogin%2Fcallback&response_type=code&scope=identify';
  }

  public render() {
    return (
      <div className={style.container}>
        <div className={style.loginContainer}>
          <div className={style.logoContainer}>
            <Logo className={style.logo} viewBox="0 0 508 508" />
            <h1>Omni Reborn</h1>
          </div>
          <button onClick={() => this.login()}>
            Login through Discord
          </button>
        </div>
      </div>
    );
  }
}
