import * as React from 'react';
import * as style from './style.css';
import { inject, observer } from 'mobx-react';
import { STORE_ROUTER } from 'app/constants';
import { RouterStore } from 'app/stores';
import MegaphoneRobot from 'assets/images/robot-megaphone.svg';

@inject(STORE_ROUTER)
@observer
export class ForbiddenPage extends React.Component {
  public render() {
    const routerStore = this.props[STORE_ROUTER] as RouterStore;
    return (
      <div className={style.container}>
        <div className={style.contentContainer}>
          <div className={style.infoContainer}>
            <MegaphoneRobot className={style.image} viewBox="0 0 153.4 195" />
            <div className={style.textContainer}>
              <span>403 Forbidden</span>
              <span>It seems you do not have</span>
              <span>the permissions to this page.</span>
              <span>Please, go back!</span>
              <button onClick={() => routerStore.replace('/dashboard')}>To Dashboard</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
