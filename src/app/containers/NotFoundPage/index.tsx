import * as React from 'react';
import * as style from './style.css';
import { inject, observer } from 'mobx-react';
import { STORE_ROUTER } from 'app/constants';
import { RouterStore } from 'app/stores';
import SleepingRobot from 'assets/images/robot-sleeping.svg';

@inject(STORE_ROUTER)
@observer
export class NotFoundPage extends React.Component {
  public render() {
    const routerStore = this.props[STORE_ROUTER] as RouterStore;
    return (
      <div className={style.container}>
        <div className={style.contentContainer}>
          <div className={style.infoContainer}>
            <SleepingRobot className={style.image} viewBox="0 0 108.5 206.6" />
            <div className={style.textContainer}>
              <span>404 Not Found</span>
              <span>It seems the page you</span>
              <span>are trying to access does</span>
              <span>not exist.</span>
              <span>Please, go back!</span>
              <button onClick={() => routerStore.replace('/dashboard')}>To Dashboard</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
