import * as React from 'react';
import * as classnames from 'classnames/bind';
import * as style from './style.css';
import qs from 'qs';
import Logo from 'assets/images/owl.svg';
import { STORE_ROUTER } from 'app/constants';
import { observer, inject } from 'mobx-react';
import { API } from 'app/api';
import { RouterStore } from 'app/stores';

const cx = classnames.bind(style);

@inject(STORE_ROUTER)
@observer
export class LoginCallbackPage extends React.Component {
  public async login() {
    const routerStore = this.props[STORE_ROUTER] as RouterStore;
    const code = qs.parse(routerStore.location.search, { ignoreQueryPrefix: true }).code;
    await API.post('/user/login/client', { code });
  }

  /* tslint:disable: no-floating-promises */
  public componentDidMount() {
    this.login();
  }

  public render() {
    const loadingClassnames = cx({
      loadingAnimation: true,
      failed: true,
    });

    return (
      <div className={style.container}>
        <div className={style.loginContainer}>
          <div className={style.logoContainer}>
            <div className={loadingClassnames} />
            <Logo className={style.logo} viewBox="0 0 508 508" />
            <h1>Authenticating…</h1>
          </div>
        </div>
      </div>
    );
  }
}
